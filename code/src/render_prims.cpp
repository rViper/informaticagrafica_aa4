#include <GL\glew.h>
#include <glm\gtc\type_ptr.hpp>
#include <glm\gtc\matrix_transform.hpp>

// Boolean variables allow to show/hide the primitives
bool renderSphere = true;
bool renderCapsule = true;
bool renderParticles = false;
bool renderCloth = false;
bool renderCube = true;

void setupPrims()
{
}

void cleanupPrims()
{
}

void renderPrims()
{
}
